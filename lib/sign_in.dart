import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import 'service/auth_service.dart';
import 'service/resultat_service.dart';

class SignInPage extends StatelessWidget {
  final _resultatService = ResultatRepository();
  final _authService = AuthRepository();

  final emailCtrl = TextEditingController();
  final pwdCtrl = TextEditingController();

  SignInPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: _buildBody(context),
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height - 100,
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildHeader(),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 40),
            child: Column(
              children: [
                _buildInputField(label: "Email", controller: emailCtrl, onSubmitted: (value) { _handleSignIn(context); }),
                _buildInputField(
                  label: "Mot de passe",
                  controller: pwdCtrl,
                  obscureText: true,
                  textInputAction: TextInputAction.done,
                  onSubmitted: (value) { _handleSignIn(context); },
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 40),
            child: FilledButton(
              style: FilledButton.styleFrom(
                minimumSize: const Size.fromHeight(50),
              ),
              onPressed: () => _handleSignIn(context),
              child: const Text('Se connecter'),
            ),
          ),
          const SizedBox(height: 20),
          _buildSignUpLink(context),
        ],
      ),
    );
  }

  Widget _buildHeader() {
    return Column(
      children: [
        const Text(
          "Login",
          style: TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(height: 20),
        Text(
          "Bienvenue sur Prono Miss France",
          style: TextStyle(
            fontSize: 15,
            color: Colors.grey[700],
          ),
        ),
        const SizedBox(height: 30),
      ],
    );
  }

  Widget _buildInputField({
    required String label,
    required TextEditingController controller,
    bool obscureText = false,
    TextInputAction textInputAction = TextInputAction.next,
    required Function(String) onSubmitted,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: const TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.w400,
            color: Colors.black87,
          ),
        ),
        const SizedBox(height: 5),
        TextField(
          controller: controller,
          textInputAction: textInputAction,
          obscureText: obscureText,
          onSubmitted: onSubmitted,
          decoration: const InputDecoration(
            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color.fromARGB(255, 189, 189, 189),
              ),
            ),
            border: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color.fromARGB(255, 189, 189, 189),
              ),
            ),
          ),
        ),
        const SizedBox(height: 30),
      ],
    );
  }

  Widget _buildSignUpLink(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text("Pas encore de compte ?"),
        const SizedBox(width: 10),
        InkWell(
          child: const Text(
            "Créer un compte",
            style: TextStyle(
              color: Color.fromARGB(255, 99, 21, 86),
              fontWeight: FontWeight.w600,
              fontSize: 18,
            ),
          ),
          onTap: () {
            context.push('/signup');
          },
        ),
      ],
    );
  }

  void _handleSignIn(BuildContext context) async {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return const Dialog(
          elevation: 0,
          backgroundColor: Colors.transparent,
          child: Center(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                CircularProgressIndicator(),
              ],
            ),
          ),
        );
      },
    );

    try {
      await _authService.signIn(emailCtrl.text, pwdCtrl.text).then((value) {
          var user = _authService.getCurrentUser()!;
          _resultatService.initResultat(user.email!, user.displayName!);

          Navigator.pop(context);
          context.go('/');
          ScaffoldMessenger.of(context).clearSnackBars();
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Row(
              children: [
                const Icon(Icons.check),
                const SizedBox(width: 10.0),
                Text(
                  "Bonjour ${_authService.getCurrentUser()?.displayName}",
                  style: TextStyle(color: Colors.grey[800]),
                )
              ],
            ),
            duration: const Duration(seconds: 2),
            backgroundColor: Colors.green[200],
            showCloseIcon: true,
            closeIconColor: Colors.grey[800],
            padding: const EdgeInsets.all(10),
            behavior: SnackBarBehavior.floating,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
          ));
      });
    } on FirebaseAuthException catch (e) {
      if (context.mounted) {
        Navigator.pop(context);
        showError(context, errorMessage: e.message);
      }
    }
  }

  void showError(BuildContext context,
      {String? errorMessage = "Erreur de connexion"}) {
    ScaffoldMessenger.of(context).clearSnackBars();
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Row(
        children: [
          const Icon(Icons.warning),
          const SizedBox(width: 10.0),
          Flexible(
            child: Text(
              errorMessage!,
              style: TextStyle(color: Colors.grey[800]),
            ),
          ),
        ],
      ),
      duration: const Duration(seconds: 3),
      backgroundColor: Colors.red[200],
      showCloseIcon: true,
      closeIconColor: Colors.grey[800],
      padding: const EdgeInsets.all(10),
      behavior: SnackBarBehavior.floating,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
    ));
  }
}
