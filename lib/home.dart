import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:missfranceprono/classement.dart';
import 'package:missfranceprono/accueil.dart';
import 'package:missfranceprono/info_utilisateur.dart';
import 'package:missfranceprono/mes_pronos.dart';
import 'package:missfranceprono/mes_resultats.dart';
import 'package:missfranceprono/vote_diapo.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'vote_final.dart';
import 'model/pronos.dart';
import 'mes_shared_preferences.dart';
import 'service/pronos_service.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _pronosService = PronosRepository();
  
  bool _isDisableMode = false;
  int _currentMode = 0;
  int _selectedPictureIndex = 0;

  late SharedPreferencesNotifier notifier;
  late StreamSubscription<DocumentSnapshot<Map<String, dynamic>>>
      subscriptionMode;

  // Liste des pages du bottom
  static final List<Widget> _widgets = <Widget>[
    const AccueilPage(),
    const MesPronosPage(),
    const MesResultatsPage(),
    const ClassementPage()
  ];

  void _startListenMode() {
    subscriptionMode = FirebaseFirestore.instance
        .collection('concours')
        .doc('missfrance2023')
        .snapshots()
        .listen((value) async {
      var mode = value.data()!['mode'] as int;
      var blocage = value.data()!['blocage'] as bool;

      //Reset de la liste mémorisée des pronos
      SharedPreferences prefs = await SharedPreferences.getInstance();
      var blocageMode = await _handleBlocageMode(mode);

      //Blocage forcé
      if (blocage) {
        notifier.setBlocage(blocage);
      } else {
        if (_currentMode != mode && !blocageMode) {
          prefs.remove('selectedCandidates');
        }
        notifier.setBlocage(blocageMode);
      }

      notifier.setMode(mode);
    });
  }

  Future<bool> _handleBlocageMode(int mode) async {
    var pronosDoc = await _pronosService.getPronos();
    if (pronosDoc.exists) {
      var pronos = Pronos.fromJson(pronosDoc.data() as Map<String, dynamic>);
      // Determine si on a envoyé ses pronos
      return (mode == 0 &&
              pronos.premierTour != null &&
              pronos.premierTour!.isNotEmpty) ||
          (mode == 1 &&
              pronos.deuxiemeTour != null &&
              pronos.deuxiemeTour!.isNotEmpty) ||
          (mode == 2 &&
              pronos.missFrance != null &&
              pronos.missFrance!.isNotEmpty);
    }

    return false;
  }

  void _onItemTapped(int index) {
    setState(() {
      if (index < _widgets.length) _selectedPictureIndex = index;
    });
  }

  void _updateData() {
    setState(() {
      _currentMode = notifier.mode;
      _isDisableMode = notifier.isBlocage;
    });
  }

  @override
  void initState() {
    notifier = Provider.of<SharedPreferencesNotifier>(context, listen: false);
    super.initState();

    //Récupération du mode
    FirebaseFirestore.instance
        .collection('concours')
        .doc('missfrance2023')
        .get()
        .then((value) {
      var mode = value.data()!['mode'] as int;
      var blocage = value.data()!['blocage'] as bool;
      notifier.setMode(mode);
      notifier.setBlocage(blocage);
      _startListenMode();
    });

    _updateData();
    notifier.addListener(_updateData);
  }

  @override
  void dispose() {
    notifier.removeListener(_updateData);
    subscriptionMode.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: _widgets.elementAt(_selectedPictureIndex),
      floatingActionButtonLocation: FloatingActionButtonLocation.miniEndFloat,
      floatingActionButton: _buildFloatingActionButton(context),
      bottomNavigationBar: _buildBottomNavigationBar(),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      title: const Text("Prono miss france"),
      backgroundColor: Colors.white,
      elevation: 0,
      foregroundColor: Colors.grey[800],
      actions: [
        IconButton(
          onPressed: () async {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => const InfoUtilisateurPage()));
          },
          icon: const Icon(Icons.account_circle),
        ),
      ],
    );
  }

  Widget? _buildFloatingActionButton(BuildContext context) {
    return _selectedPictureIndex > 0 || _currentMode > 2
        ? null
        : FloatingActionButton.extended(
            onPressed: _isDisableMode
                ? null
                : () {
                    _handleFloatingActionButtonPressed(context);
                  },
            foregroundColor: _isDisableMode
                ? Colors.white
                : const Color.fromARGB(255, 112, 8, 95),
            backgroundColor: _isDisableMode
                ? const Color.fromARGB(255, 150, 150, 150)
                : Colors.pink[200],
            icon: Icon(_isDisableMode ? Icons.timer_sharp : Icons.check),
            label: Text(
                _isDisableMode ? 'En attente des résultats' : 'Pronostiquer'),
          );
  }

  void _handleFloatingActionButtonPressed(BuildContext context) {
    int numberVote = _currentMode == 0 ? 15 : 5;
    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => _currentMode < 2
          ? VoteDiapoPage(numberVote: numberVote)
          : const VoteFinalPage(),
    ));
  }

  NavigationBar _buildBottomNavigationBar() {
    return NavigationBar(
      destinations: const <NavigationDestination>[
        NavigationDestination(
          icon: Icon(Icons.person_3_rounded),
          label: 'Candidates',
        ),
        NavigationDestination(
          icon: Icon(Icons.add_task),
          label: 'Mes pronos',
        ),
        NavigationDestination(
          icon: Icon(Icons.account_balance),
          label: 'Mes résultats',
        ),
        NavigationDestination(
          icon: Icon(Icons.list_alt),
          label: 'Classement',
        ),
      ],
      selectedIndex: _selectedPictureIndex,
      height: 50,
      onDestinationSelected: _onItemTapped,
    );
  }
}
