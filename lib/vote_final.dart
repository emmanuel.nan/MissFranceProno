import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:missfranceprono/model/pronos.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'candidate.dart';
import 'model/candidate.dart';
import 'mes_shared_preferences.dart';
import 'service/candidate_service.dart';
import 'service/pronos_service.dart';

class VoteFinalPage extends StatefulWidget {
  const VoteFinalPage({super.key});

  @override
  State<VoteFinalPage> createState() => _VoteFinalPageState();
}

class _VoteFinalPageState extends State<VoteFinalPage> {
  final _candidateService = CandidateRepository();
  final _pronosService = PronosRepository();
  var _candidates = List<Candidate>.empty();

  late SharedPreferencesNotifier notifier;

  void _initializeCandidate() async {
    var response = await _candidateService.getCandidates(false);

    setState(() {
      _candidates = response.docs
          .map((doc) => Candidate.fromJson(doc.data() as Map<String, dynamic>))
          .toList();
    });
  }

  @override
  void initState() {
    notifier = Provider.of<SharedPreferencesNotifier>(context, listen: false);
    super.initState();
    _initializeCandidate();
  }

  Future<void> saveSelectedCandidates(List<Candidate> candidates) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> selectedCandidatesJson =
        candidates.map((candidate) => jsonEncode(candidate.toJson())).toList();

    prefs.setStringList('selectedCandidates', selectedCandidatesJson);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            _buildHeader("Candidates VoteFinalistes :", 20),
            _buildHeader(
              "Faire glisser votre sélection dans l'ordre :",
              14,
              isItalic: true,
            ),
            Expanded(
              child: _buildReorderableListView(),
            ),
            _buildSelectionContainer(),
          ],
        ),
      ),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      title: const Text("Miss France Prono"),
      elevation: 0,
      foregroundColor: Colors.grey[800],
      backgroundColor: Colors.white,
    );
  }

  Widget _buildHeader(String text, double fontSize, {bool isItalic = false}) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 5, 10, 2),
      child: Text(
        text,
        style: TextStyle(
          color: Colors.grey[800],
          fontWeight: FontWeight.bold,
          fontSize: fontSize,
          fontStyle: isItalic ? FontStyle.italic : FontStyle.normal,
        ),
      ),
    );
  }

  Widget _buildReorderableListView() {
    return ReorderableListView.builder(
      key: UniqueKey(),
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      padding: const EdgeInsets.symmetric(horizontal: 0),
      itemCount: _candidates.length,
      itemBuilder: (context, index) {
        var candidate = _candidates[index];

        var name = candidate.nom!;
        var region = candidate.region!;
        var url = candidate.url![0];

        return ReorderableDragStartListener(
          index: index,
          key: ValueKey(index),
          child: ListTile(
            leading: Hero(
              tag: name,
              child: CircleAvatar(
                radius: 35,
                backgroundImage: NetworkImage(url),
              ),
            ),
            title: Text(
              name,
              style: const TextStyle(
                fontWeight: FontWeight.w500,
              ),
            ),
            subtitle: Text(region),
            trailing: const Icon(
              Icons.reorder,
              color: Colors.grey,
            ),
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => CandidatePage(candidate: candidate),
                ),
              );
            },
          ),
        );
      },
      onReorder: (int oldIndex, int newIndex) {
        setState(() {
          if (oldIndex < newIndex) {
            newIndex -= 1;
          }
          dynamic item = _candidates.removeAt(oldIndex);
          _candidates.insert(newIndex, item);
        });
      },
    );
  }

  Widget _buildSelectionContainer() {
    return Container(
      margin: const EdgeInsets.only(top: 50),
      decoration: const BoxDecoration(
        color: Color.fromARGB(175, 99, 21, 86),
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(25),
        ),
      ),
      height: 150,
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildSelectionText("Miss France",
                _candidates.isNotEmpty ? _candidates[0].nom! : " - "),
            _buildSelectionText("Deuxième dauphine",
                _candidates.isNotEmpty ? _candidates[1].nom! : " - "),
            _buildSelectionText("Troisième dauphine",
                _candidates.isNotEmpty ? _candidates[2].nom! : " - "),
            ElevatedButton(
              onPressed: _validateSelection,
              child: const Center(
                child: Text('Valider'),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildSelectionText(String label, String value) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: Text(
        "$label : $value",
        style: const TextStyle(color: Colors.white),
      ),
    );
  }

  void _validateSelection() async {
    saveSelectedCandidates(_candidates.take(3).toList());
    await _pronosService.pushPronos(Pronos(
      missFrance: _candidates[0].nom,
      deuxiemeDauphine: _candidates[1].nom,
      troisiemeDauphine: _candidates[2].nom,
    ));

    notifier.setBlocage(true);

    if(context.mounted) {
      ScaffoldMessenger.of(context).clearSnackBars();
      Navigator.of(context).pop();

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Row(
          children: [
            const Icon(Icons.check),
            const SizedBox(
              width: 10.0,
            ),
            Text(
              "Pronostics enregistrés",
              style: TextStyle(color: Colors.grey[800]),
            ),
          ],
        ),
        duration: const Duration(seconds: 1),
        backgroundColor: Colors.green[200],
        showCloseIcon: true,
        closeIconColor: Colors.grey[800],
        padding: const EdgeInsets.all(10),
        behavior: SnackBarBehavior.floating,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
      ));
    }
  }
}
