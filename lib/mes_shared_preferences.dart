import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesNotifier extends ChangeNotifier {
  SharedPreferences? _prefs;

  SharedPreferencesNotifier() {
    _initSharedPreferences(); // Initialise _prefs ici dans le constructeur
  }

  Future<void> _initSharedPreferences() async {
    _prefs = await SharedPreferences.getInstance();
    notifyListeners(); // Vous pouvez également appeler notifyListeners ici si nécessaire
  }

bool get isBlocage {
  
      return _prefs?.getBool("blocage") ?? false;
    
  }

  Future<void> setBlocage(bool newValue) async {
    await _prefs?.setBool("blocage", newValue);
    notifyListeners();
  }
  
  int get mode {
  
      return _prefs?.getInt("mode") ?? 0;
    
  }
  
  Future<void> setMode(int newValue) async {
    await _prefs?.setInt("mode", newValue);
    notifyListeners();
  }
}