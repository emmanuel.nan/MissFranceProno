import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:missfranceprono/service/auth_service.dart';

class SignUpPage extends StatelessWidget {
  final _authService = AuthRepository();

  final emailCtrl = TextEditingController();
  final pseudoCtrl = TextEditingController();
  final pwdCtrl = TextEditingController();
  final pwdCheckCtrl = TextEditingController();

  SignUpPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.white,
      appBar: _buildAppBar(context),
      body: SafeArea(
        child: SingleChildScrollView(
          child: _buildBody(context),
        ),
      ),
    );
  }

  AppBar _buildAppBar(BuildContext context) {
    return AppBar(
      scrolledUnderElevation: 0.0,
      elevation: 0,
      backgroundColor: Colors.transparent,
      leading: IconButton(
        onPressed: () {
          context.pop();
        },
        icon: const Icon(
          Icons.arrow_back_ios,
          size: 20,
          color: Colors.black,
        ),
      ),
      systemOverlayStyle: SystemUiOverlayStyle.dark,
    );
  }

  Widget _buildBody(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height - 100,
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          _buildHeader(),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 40),
            child: Column(
              children: [
                _buildInputField(label: "Pseudo", controller: pseudoCtrl),
                _buildInputField(label: "Email", controller: emailCtrl),
                _buildInputField(
                  label: "Mot de passe",
                  controller: pwdCtrl,
                  obscureText: true,
                ),
                _buildInputField(
                  label: "Confirmez mot de passe",
                  controller: pwdCheckCtrl,
                  obscureText: true,
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 40),
            child: FilledButton(
              style: FilledButton.styleFrom(
                minimumSize: const Size.fromHeight(50),
              ),
              onPressed: () => _handleSignUp(context),
              child: const Text("Créer un compte"),
            ),
          ),
          const SizedBox(height: 10),
          _buildSignInLink(context),
        ],
      ),
    );
  }

  Widget _buildHeader() {
    return Column(
      children: [
        const Text(
          "S'enregistrer",
          style: TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(height: 10),
        Text(
          "Créer un compte, c'est gratuit",
          style: TextStyle(
            fontSize: 15,
            color: Colors.grey[700],
          ),
        ),
        const SizedBox(height: 10),
      ],
    );
  }

  Widget _buildInputField({
    required String label,
    required TextEditingController controller,
    bool obscureText = false,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: const TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.w400,
            color: Colors.black87,
          ),
        ),
        const SizedBox(height: 5),
        TextField(
          controller: controller,
          obscureText: obscureText,
          decoration: const InputDecoration(
            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color.fromARGB(255, 189, 189, 189),
              ),
            ),
            border: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color.fromARGB(255, 189, 189, 189),
              ),
            ),
          ),
        ),
        const SizedBox(height: 10),
      ],
    );
  }

  void _handleSignUp(BuildContext context) async {
    var email = emailCtrl.text.toLowerCase();

    if (!isValidEmail(email)) {
      showError(context, errorMessage: "Le format de l'email n'est pas valide");
    } else if (!isPasswordsEqual()) {
      showError(context,
          errorMessage: 'Les mots de passe ne sont pas identiques');
    } else {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return const Dialog(
            elevation: 0,
            backgroundColor: Colors.transparent,
            child: Center(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  CircularProgressIndicator(),
                ],
              ),
            ),
          );
        },
      );

      try {
        await _authService
            .signUp(email, pwdCtrl.text, pseudoCtrl.text)
            .then((value) {
          Navigator.pop(context);
          context.go('/signin');
          ScaffoldMessenger.of(context).clearSnackBars();
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Row(
              children: [
                const Icon(Icons.check),
                const SizedBox(width: 10.0),
                Expanded(
                  child: Text(
                    "Votre compte a été créé. Un email a été envoyé sur ${emailCtrl.text}",
                    style: TextStyle(color: Colors.grey[800]),
                  ),
                ),
              ],
            ),
            duration: const Duration(seconds: 3),
            backgroundColor: Colors.green[200],
            showCloseIcon: true,
            closeIconColor: Colors.grey[800],
            padding: const EdgeInsets.all(10),
            behavior: SnackBarBehavior.floating,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
          ));
        });
      } on FirebaseAuthException catch (e) {
        if(context.mounted) {
          Navigator.pop(context);
          showError(context, errorMessage: e.message);
        }
      }
    }
  }

  Widget _buildSignInLink(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text("Vous avez déjà un compte ? "),
        InkWell(
          child: const Text(
            "Se connecter",
            style: TextStyle(
              color: Colors.pink,
              fontWeight: FontWeight.w600,
              fontSize: 18,
            ),
          ),
          onTap: () {
            context.go('/signin');
          },
        ),
      ],
    );
  }

  void showError(BuildContext context,
      {String? errorMessage = "Erreur de connexion"}) {
    ScaffoldMessenger.of(context).clearSnackBars();
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Row(
        children: [
          const Icon(Icons.warning),
          const SizedBox(width: 10.0),
          Flexible(
            child: Text(
              errorMessage!,
              style: TextStyle(color: Colors.grey[800]),
            ),
          ),
        ],
      ),
      duration: const Duration(seconds: 3),
      backgroundColor: Colors.red[200],
      showCloseIcon: true,
      closeIconColor: Colors.grey[800],
      padding: const EdgeInsets.all(10),
      behavior: SnackBarBehavior.floating,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
    ));
  }

  bool isPasswordsEqual() {
    return pwdCtrl.text == pwdCheckCtrl.text;
  }

  bool isValidEmail(String email) {
    // Définir une expression régulière pour valider le format de l'e-mail
    RegExp emailRegex = RegExp(r'^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$');

    // Vérifier si l'e-mail correspond à l'expression régulière
    return emailRegex.hasMatch(email);
  }
}
