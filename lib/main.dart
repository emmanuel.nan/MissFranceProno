import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:go_router/go_router.dart';
import 'package:missfranceprono/sign_in.dart';
import 'package:missfranceprono/sign_up.dart';
import 'package:provider/provider.dart';
import 'firebase_options.dart';
import 'home.dart';
import 'mes_shared_preferences.dart';
import 'service/auth_service.dart';

// Define a separate class for handling background notifications
class NotificationHandler {
  static void handleBackgroundNotification(NotificationResponse notificationResponse) {
        print("Notification tapped with payload: ${notificationResponse.payload}");
  }
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  int id = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routerConfig: _buildRouter(),
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorSchemeSeed: const Color.fromARGB(255, 99, 21, 86),
        useMaterial3: true,
      ),
    );
  }

  GoRouter _buildRouter() {
    return GoRouter(
      routes: [
        _buildHomeRoute(),
        _buildSignInRoute(),
        _buildSignUpRoute(),
      ],
    );
  }

  GoRoute _buildHomeRoute() {
    return GoRoute(
      name: "home",
      path: "/",
      builder: (context, state) => const HomePage(),
      redirect: (context, state) {
        if (AuthRepository().getCurrentUser() == null) {
          return "/signin";
        }
        return "/";
      },
    );
  }

  GoRoute _buildSignInRoute() {
    return GoRoute(
      name: "signin",
      path: "/signin",
      builder: (context, state) => SignInPage(),
    );
  }

  GoRoute _buildSignUpRoute() {
    return GoRoute(
      name: "signup",
      path: "/signup",
      builder: (context, state) => SignUpPage(),
    );
  }

  @override
  void initState() {
    super.initState();
    _initializeFirebase();
    _initializeLocalNotifications();
  }

  Future<void> _configureFirebaseMessaging(BuildContext context) async {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print("Message received: ${message.notification?.title}");
      print("Data: ${message.data}");
      _showNotification(
        message.notification?.title ?? '',
        message.notification?.body ?? '',
      );
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print("Message opened app: ${message.notification?.title}");
      print("Data: ${message.data}");
      _showNotification(
        message.notification?.title ?? '',
        message.notification?.body ?? '',
      );
    });

    String? token = await _firebaseMessaging.getToken();
    print("Device Token: $token");
  }

  Future<void> _initializeLocalNotifications() async {
    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('app_icon');
    const InitializationSettings initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid,
    );

    await flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
      onDidReceiveNotificationResponse:
          (NotificationResponse notificationResponse) {
        print("Notification tapped with payload: ${notificationResponse.payload}");
      },
      onDidReceiveBackgroundNotificationResponse: NotificationHandler.handleBackgroundNotification,
    );
  }

  Future<void> _initializeFirebase() async {
    await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  }

  void _showNotification(String title, String body) async {
    const AndroidNotificationDetails androidNotificationDetails =
        AndroidNotificationDetails(
      'pronomissfrance-id',
      'pronomissfrance-nom',
      channelDescription: 'pronomissfrance-desc',
      importance: Importance.max,
      priority: Priority.high,
      ticker: 'ticker',
    );
    const NotificationDetails notificationDetails =
        NotificationDetails(android: androidNotificationDetails);
    await flutterLocalNotificationsPlugin.show(
      id++,
      title,
      body,
      notificationDetails,
      payload: 'item x',
    );
  }
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(); // Assurez-vous d'initialiser Firebase avant d'utiliser ses fonctionnalités

  runApp(
    ChangeNotifierProvider(
      create: (context) => SharedPreferencesNotifier(),
      child: const MyApp(),
    ),
  );
}