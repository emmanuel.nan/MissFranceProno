import 'package:flutter/material.dart';
import 'package:missfranceprono/service/admin_service.dart';

import 'candidate.dart';
import 'model/candidate.dart';
import 'service/candidate_service.dart';

class AdminVotePage extends StatefulWidget {
  const AdminVotePage({super.key, required this.numberVote});

  final int numberVote;

  @override
  State<AdminVotePage> createState() => _AdminVotePageState();
}

class _AdminVotePageState extends State<AdminVotePage> {
  final ScrollController _scrollController = ScrollController();
  final CandidateRepository _candidateService = CandidateRepository();
  final AdminRepository _adminService = AdminRepository();
  final List<Candidate> _selectedMiss = [];
  final List<Candidate> _candidates = [];

  @override
  void initState() {
    super.initState();
    _initializeCandidates();
  }

  void _initializeCandidates() async {
    var response = await _candidateService.getCandidates(false);
    setState(() {
      _candidates.addAll(response.docs.map(
          (doc) => Candidate.fromJson(doc.data() as Map<String, dynamic>)));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: _buildCandidateList(),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      title: Text("${_selectedMiss.length}/${widget.numberVote}"),
      elevation: 0,
      foregroundColor: Colors.grey[800],
      backgroundColor: Colors.white,
      actions: [
        IconButton(
          icon: const Icon(Icons.check),
          onPressed: _selectedMiss.length == widget.numberVote
              ? () => _validatePronos()
              : null,
        ),
      ],
    );
  }

  Widget _buildCandidateList() {
    return ListView.builder(
      controller: _scrollController,
      itemCount: _candidates.length,
      itemBuilder: (BuildContext context, int index) {
        return _buildCandidateItem(index);
      },
    );
  }

  Widget _buildCandidateItem(int index) {
    var candidate = _candidates[index];
    var isSelected = _selectedMiss.contains(candidate);

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
      child: ListTile(
        leading: _buildLeading(isSelected, candidate.url![0]),
        title: Text(candidate.nom!,
            style: const TextStyle(fontWeight: FontWeight.w500)),
        subtitle: Text(candidate.region!),
        selected: isSelected,
        onTap: () => _onCandidateTap(candidate, isSelected),
        onLongPress: () => _onCandidateLongPress(candidate),
      ),
    );
  }

  Widget _buildLeading(bool isSelected, String url) {
    return GestureDetector(
      child: SizedBox(
        width: 70.0,
        child: AnimatedSwitcher(
          duration: const Duration(milliseconds: 500),
          transitionBuilder: (Widget child, Animation<double> animation) {
            return ScaleTransition(
              scale: animation,
              alignment: Alignment.center,
              filterQuality: FilterQuality.medium,
              child: child,
            );
          },
          child: isSelected
              ? SizedBox(
                  height: 40.0,
                  width: 40.0,
                  child: IconButton(
                    icon: const Icon(Icons.check),
                    onPressed: null,
                    style: _enabledFilledButtonStyle(
                        isSelected, Theme.of(context).colorScheme),
                  ),
                )
              : CircleAvatar(
                  radius: 35,
                  backgroundImage: NetworkImage(url),
                ),
        ),
      ),
    );
  }

  void _onCandidateTap(Candidate candidate, bool isSelected) {
    if (!isSelected) {
      if (_selectedMiss.length < widget.numberVote) {
        setState(() {
          _selectedMiss.add(candidate);
        });
      } else {
        _showSnackBar(
            "Vous avez déjà sélectionné ${widget.numberVote} candidates",
            Colors.amber[200]!);
      }
    } else {
      setState(() {
        _selectedMiss.remove(candidate);
      });
    }
  }

  void _onCandidateLongPress(Candidate candidate) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => CandidatePage(candidate: candidate,),
      ),
    );
  }

  void _validatePronos() async {
    if (widget.numberVote == 15) {
      await _adminService.validerPremiereEtape(_selectedMiss);
    } else if (widget.numberVote == 5) {
      await _adminService.validerDeuxiemeEtape(_selectedMiss);
    }

    if (context.mounted) {
      ScaffoldMessenger.of(context).clearSnackBars();
      Navigator.of(context).pop();
    }

    _showSnackBar("Pronostics enregistrés", Colors.green[200]!);
  }

  void _showSnackBar(String message, Color backgroundColor) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Row(
          children: [
            const Icon(Icons.check),
            const SizedBox(width: 10.0),
            Expanded(
              child: Text(
                message,
                style: TextStyle(color: Colors.grey[800]),
              ),
            ),
          ],
        ),
        duration: const Duration(seconds: 1),
        backgroundColor: backgroundColor,
        showCloseIcon: true,
        closeIconColor: Colors.grey[800],
        padding: const EdgeInsets.all(10),
        behavior: SnackBarBehavior.floating,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
      ),
    );
  }

  ButtonStyle _enabledFilledButtonStyle(bool isSelected, ColorScheme colors) {
    return IconButton.styleFrom(
      foregroundColor: isSelected ? colors.onPrimary : colors.primary,
      backgroundColor: isSelected ? colors.primary : colors.surfaceVariant,
      disabledForegroundColor: colors.onSurface.withOpacity(0.38),
      disabledBackgroundColor: colors.onSurface.withOpacity(0.12),
      hoverColor: isSelected
          ? colors.onPrimary.withOpacity(0.08)
          : colors.primary.withOpacity(0.08),
      focusColor: isSelected
          ? colors.onPrimary.withOpacity(0.12)
          : colors.primary.withOpacity(0.12),
      highlightColor: isSelected
          ? colors.onPrimary.withOpacity(0.12)
          : colors.primary.withOpacity(0.12),
    );
  }
}
