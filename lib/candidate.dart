import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:missfranceprono/model/candidate.dart';

class CandidatePage extends StatefulWidget {
  const CandidatePage({super.key, required this.candidate});
  final Candidate candidate;

  @override
  State<CandidatePage> createState() => _CandidatePageState();
}

class _CandidatePageState extends State<CandidatePage> {
  late Candidate candidate;
  int currentIndex = 0;

  @override
  void initState() {
    candidate = widget.candidate;
    super.initState();
  }

  void _next() {
    setState(() {
      currentIndex = currentIndex < candidate.url!.length - 1
          ? currentIndex + 1
          : currentIndex;
    });
  }

  void _prev() {
    setState(() {
      currentIndex = currentIndex > 0 ? currentIndex - 1 : 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget> imageSliders = _buildImageSliders();

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: _buildAppBar(),
      body: _buildBody(imageSliders),
    );
  }

  List<Widget> _buildImageSliders() {
    return candidate.url!
        .map<Widget>(
          (item) => Container(
            child: Image(
              width: double.infinity,
              fit: BoxFit.cover,
              image: NetworkImage(item),
            ),
          ),
        )
        .toList();
  }

  AppBar _buildAppBar() {
    return AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0,
      foregroundColor: Colors.grey,
    );
  }

  Widget _buildBody(List<Widget> imageSliders) {
    double screenHeight = MediaQuery.of(context).size.height;

    return GestureDetector(
      onHorizontalDragEnd: (DragEndDetails details) {
        if (details.velocity.pixelsPerSecond.dx > 0) {
          _prev();
        } else if (details.velocity.pixelsPerSecond.dx < 0) {
          _next();
        }
      },
      child: Column(
        children: [
          _buildCarouselSlider(imageSliders, screenHeight),
          _buildIndicatorRow(),
          _buildCandidateInfo(),
        ],
      ),
    );
  }

  Widget _buildCarouselSlider(List<Widget> imageSliders, double screenHeight) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 4,
            offset: const Offset(0, 3),
          ),
        ],
      ),
      child: ClipRRect(
        borderRadius: const BorderRadius.only(
          bottomLeft: Radius.circular(20.0),
          bottomRight: Radius.circular(20.0),
        ),
        child: CarouselSlider(
          options: CarouselOptions(
            height: screenHeight * 0.7,
            aspectRatio: 1,
            viewportFraction: 1,
            enableInfiniteScroll: false,
            initialPage: 0,
            autoPlay: false,
            enlargeCenterPage: false,
            onPageChanged: (index, reason) {
              setState(() {
                currentIndex = index;
              });
            },
          ),
          items: imageSliders,
        ),
      ),
    );
  }

  Widget _buildIndicatorRow() {
    return Container(
      width: 90,
      margin: const EdgeInsets.only(top: 20, bottom: 10),
      child: Row(
        children: _buildIndicator(),
      ),
    );
  }

  Widget _buildCandidateInfo() {
    return Padding(
      padding: const EdgeInsets.only(left: 20),
      child: Align(
        alignment: Alignment.topLeft,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              candidate.nom ?? '',
              softWrap: true,
              style: TextStyle(
                color: Colors.grey[800],
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              candidate.region ?? '',
              softWrap: true,
              style: TextStyle(color: Colors.grey[800], fontSize: 15),
            ),
            Text(
              'Elue le: ${candidate.elue ?? ""}',
              softWrap: true,
              style: TextStyle(color: Colors.grey[800], fontSize: 15),
            ),
            Text(
              'Age: ${candidate.age ?? ""}',
              softWrap: true,
              style: TextStyle(color: Colors.grey[800], fontSize: 15),
            ),
            Text(
              'Taille: ${candidate.taille ?? ""}',
              softWrap: true,
              style: TextStyle(color: Colors.grey[800], fontSize: 15),
            ),
          ],
        ),
      ),
    );
  }

  Widget _indicator(bool isActive) {
    return Expanded(
      child: Container(
        height: 10,
        decoration: BoxDecoration(
          border: Border.all(
            color: const Color.fromARGB(255, 66, 66, 66),
            width: 1,
          ),
          color: isActive ? Colors.grey[800] : Colors.white,
          shape: BoxShape.circle,
        ),
      ),
    );
  }

  List<Widget> _buildIndicator() {
    return List.generate(
      candidate.url!.length,
      (index) => _indicator(currentIndex == index),
    );
  }
}
