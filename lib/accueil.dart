import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:missfranceprono/admin_vote.dart';
import 'package:missfranceprono/admin_vote_final.dart';
import 'package:missfranceprono/candidate.dart';
import 'package:missfranceprono/model/candidate.dart';
import 'package:missfranceprono/service/admin_service.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'mes_shared_preferences.dart';
import 'service/auth_service.dart';
import 'service/candidate_service.dart';

class AccueilPage extends StatefulWidget {
  const AccueilPage({Key? key}) : super(key: key);

  @override
  State<AccueilPage> createState() => _AccueilPageState();
}

class _AccueilPageState extends State<AccueilPage> {
  final CandidateRepository candidateService = CandidateRepository();
  final AuthRepository authService = AuthRepository();
  final AdminRepository adminService = AdminRepository();
  late SharedPreferencesNotifier notifier;
  bool admin = false;

  @override
  void initState() {
    super.initState();
    notifier = Provider.of<SharedPreferencesNotifier>(context, listen: false);
    admin = authService.getCurrentUser()?.email == 'emmanuel.nan@gmail.com';
    notifier.addListener(_updateState);
  }

  void _updateState() {
    if (mounted) {
      setState(() {});
    }
  }

  @override
  void dispose() {
    notifier.removeListener(_updateState);
    super.dispose();
  }

  Future<QuerySnapshot> getData(bool isEliminated) async {
    return await candidateService.getCandidates(isEliminated);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (admin) ..._buildAdminButtons(),
          const Divider(),
          _buildSectionHeader("Candidates en compétition"),
          Expanded(child: _buildCandidateList(false)),
          _buildSectionHeader("Candidates éliminées"),
          Expanded(child: _buildCandidateList(true)),
        ],
      ),
    );
  }

  List<Widget> _buildAdminButtons() {
    return [
      Row(
        children: [
          Expanded(
            child: ElevatedButton(
              onPressed: () async {
                await adminService.initCandidates();
              },
              child: const Text("Reset"),
            ),
          ),
          Expanded(
            child: ElevatedButton(
              onPressed: () => _handleAdminButtonPressed(),
              child: const Text("Admin vote"),
            ),
          ),
        ],
      ),
    ];
  }

  void _handleAdminButtonPressed() async {
    BuildContext localContext = context;
    //await adminService.initCandidates();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int mode = prefs.getInt('mode') ?? 0;

    if (localContext.mounted) {
      Navigator.of(localContext).push(
        MaterialPageRoute(
          builder: (context) => mode < 2
              ? AdminVotePage(numberVote: mode == 0 ? 15 : (mode == 1 ? 5 : 0))
              : const AdminVoteFinalPage(title: "test"),
        ),
      );
    }
  }

  Widget _buildSectionHeader(String title) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
      child: Text(
        title,
        style: const TextStyle(
          color: Color.fromARGB(255, 156, 13, 132),
          fontWeight: FontWeight.bold,
          fontSize: 20,
        ),
      ),
    );
  }

  Widget _buildCandidateList(bool isEliminated) {
    return FutureBuilder<QuerySnapshot>(
      future: getData(isEliminated),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.hasData) {
          final List<DocumentSnapshot> documents = snapshot.data!.docs;
          return ListView(
            scrollDirection: Axis.horizontal,
            children: documents.map<Widget>((e) {
              var candidate =
                  Candidate.fromJson(e.data() as Map<String, dynamic>);
              if (candidate.nom == null) {
                return Container();
              }
              return SizedBox(
                width: 200,
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Card(
                    clipBehavior: Clip.antiAlias,
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                    ),
                    color: (candidate.estMissFrance ?? false)
                        ? Colors.green
                        : Colors.white,
                    elevation: 8,
                    child: InkWell(
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => CandidatePage(
                              candidate: candidate,
                            ),
                          ),
                        );
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Hero(
                            tag: candidate.nom!,
                            child: CircleAvatar(
                              radius: 50,
                              backgroundImage: NetworkImage(candidate.url![0]),
                            ),
                          ),
                          Text(
                            '${candidate.nom!}${candidate.estMissFrance! ? " - Gagnante" : ""}',
                            textAlign: TextAlign.center,
                            style: const TextStyle(
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          Text(
                            candidate.region!,
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            }).toList(),
          );
        } else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}
