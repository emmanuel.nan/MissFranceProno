import 'package:bottom_sheet_scaffold/bottom_sheet_scaffold.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:missfranceprono/service/candidate_service.dart';
import 'package:missfranceprono/service/pronos_service.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

import 'model/candidate.dart';
import 'model/pronos.dart';
import 'mes_shared_preferences.dart';

class VoteDiapoPage extends StatefulWidget {
  const VoteDiapoPage({super.key, required this.numberVote});
  final int numberVote;

  @override
  State<VoteDiapoPage> createState() => _VoteDiapoPageState();
}

class _VoteDiapoPageState extends State<VoteDiapoPage> {
  final _candidateService = CandidateRepository();
  final _pronosService = PronosRepository();

  var imageSliders = List<Widget>.empty(growable: true);
  var selectedMiss = List<Candidate>.empty(growable: true);
  var candidates = List<Candidate>.empty(growable: true);
  int currentIndex = 0;


  void _loadSelectedCandidates() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String>? selectedCandidatesJson =
        prefs.getStringList('selectedCandidates');

    if (selectedCandidatesJson != null) {
      setState(() {
        selectedMiss = selectedCandidatesJson
            .map((json) => Candidate.fromJson(jsonDecode(json)))
            .toList();
      });
    }
  }

  void _saveSelectedCandidates() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> selectedCandidatesJson = selectedMiss
        .map((candidate) => jsonEncode(candidate.toJson()))
        .toList();

    prefs.setStringList('selectedCandidates', selectedCandidatesJson);
  }


  void _initializeCandidate() async {
    var response = await _candidateService.getCandidates(false);

    setState(() {
      candidates = response.docs
          .map((doc) => Candidate.fromJson(doc.data() as Map<String, dynamic>))
          .toList();

      imageSliders = candidates
          .map((candidate) => Image(
                width: double.infinity,
                fit: BoxFit.cover,
                image: NetworkImage(candidate.url![0]),
              ))
          .toList();
    });
  }

  @override
  void initState() {
    super.initState();
    _initializeCandidate();
    _loadSelectedCandidates();
  }

  void _next() {
    setState(() {
      if (currentIndex < imageSliders.length - 1) {
        currentIndex++;
      } else {
        currentIndex = currentIndex;
      }
    });
  }

  void _preve() {
    setState(() {
      if (currentIndex > 0) {
        currentIndex--;
      } else {
        currentIndex = 0;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    var candidate =
        candidates.isNotEmpty ? candidates[currentIndex] : Candidate();
    return BottomSheetScaffold(
      bottomSheet: _buildBottomSheet(candidate),
      extendBodyBehindAppBar: true,
      appBar: _buildAppBar(context),
      body: _buildBody(context, screenHeight),
      floatingActionButtonLocation: FloatingActionButtonLocation.miniEndFloat,
      floatingActionButton: _buildFloatingActionButton()
    );
  }

  Widget? _buildFloatingActionButton() {
    return FloatingActionButton.extended(
        onPressed: () {
          if (!selectedMiss.contains(candidates[currentIndex])) {
            if (selectedMiss.length < widget.numberVote) {
              setState(() {
                selectedMiss.add(candidates[currentIndex]);
              });
            } else {
              ScaffoldMessenger.of(context).clearSnackBars();
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Row(
                  children: [
                    const Icon(Icons.warning_amber_rounded),
                    const SizedBox(
                      width: 10.0,
                    ),
                    Flexible(
                        child: Text(
                      "Vous avez déjà sélectionné ${widget.numberVote} candidates",
                      style: TextStyle(color: Colors.grey[800]),
                    ))
                  ],
                ),
                duration: const Duration(seconds: 1),
                backgroundColor: Colors.amber[200],
                showCloseIcon: true,
                closeIconColor: Colors.grey[800],
                padding: const EdgeInsets.all(10),
                behavior: SnackBarBehavior.floating,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ));
            }
          } else {
            setState(() {
              selectedMiss.remove(candidates[currentIndex]);
            });
          }
          _saveSelectedCandidates(); 
          },
          
        foregroundColor: const Color.fromARGB(255, 112, 8, 95),
        backgroundColor: Colors.pink[200],
        icon: candidates.isNotEmpty &&
                selectedMiss.contains(candidates[currentIndex])
            ? const Icon(Icons.remove)
            : const Icon(Icons.add),
        label: Text("${selectedMiss.length}/${widget.numberVote}"),
      );
  }

  DraggableBottomSheet _buildBottomSheet(Candidate candidate) {
    return DraggableBottomSheet(
      backgroundColor: const Color.fromARGB(175, 99, 21, 86),
      radius: 20,
      maxHeight: 130,
      animationDuration: const Duration(milliseconds: 200),
      body: _buildBottomSheetBody(candidate),
      header: _buildBottomSheetHeader(),
    );
  }

  Widget _buildBottomSheetBody(Candidate candidate) {
    return Container(
      width: double.infinity,
      height: 130,
      alignment: Alignment.center,
      child: Padding(
        padding: const EdgeInsets.only(left: 20),
        child: Align(
          alignment: Alignment.topLeft,
          child: _buildCandidateDetails(candidate),
        ),
      ),
    );
  }

  Widget _buildBottomSheetHeader() {
    return ClipRRect(
      borderRadius: const BorderRadius.vertical(
        top: Radius.circular(16.0),
      ),
      child: Container(
        height: 30,
        color: const Color.fromARGB(175, 99, 21, 86),
        child: const Center(
          child: Text(
            "-----",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }

  Widget _buildCandidateDetails(Candidate candidate) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          candidate.nom ?? "",
          softWrap: true,
          style: const TextStyle(
              color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold),
        ),
        Text(
          candidate.region ?? "",
          style: const TextStyle(color: Colors.white, fontSize: 15),
        ),
        Text(
          'Elue le: ${candidate.elue ?? ""}',
          softWrap: true,
          style: const TextStyle(color: Colors.white, fontSize: 15),
        ),
        Text(
          'Age: ${candidate.age ?? ""}',
          softWrap: true,
          style: const TextStyle(color: Colors.white, fontSize: 15),
        ),
        Text(
          'Taille: ${candidate.taille ?? ""}',
          softWrap: true,
          style: const TextStyle(color: Colors.white, fontSize: 15),
        ),
      ],
    );
  }

  Widget _buildBody(BuildContext context, double screenHeight) {
    return GestureDetector(
      onHorizontalDragEnd: (DragEndDetails details) {
        if (details.velocity.pixelsPerSecond.dx > 0) {
          _preve();
        } else if (details.velocity.pixelsPerSecond.dx < 0) {
          _next();
        }
      },
      child: CarouselSlider(
        options: CarouselOptions(
          height: screenHeight,
          aspectRatio: 1,
          viewportFraction: 1,
          enableInfiniteScroll: false,
          initialPage: 0,
          autoPlay: false,
          enlargeCenterPage: false,
          onPageChanged: (index, reason) {
            setState(() {
              currentIndex = index;
            });
          },
        ),
        items: imageSliders,
      ),
    );
  }

  AppBar _buildAppBar(BuildContext context) {
    return AppBar(
      title:
          Text("${candidates.isNotEmpty ? candidates[currentIndex].nom : ''}"),
      backgroundColor: const Color.fromARGB(150, 20, 20, 20),
      elevation: 100,
      foregroundColor: Colors.white,
      actions: [
        IconButton(
          onPressed: () async {
            if (selectedMiss.length != widget.numberVote) {
              _showSnackBar(context,
                  "Vous n'avez pas sélectionné les ${widget.numberVote} miss");
            } else {
              _showConfirmationDialog(context, () async {
                await _savePronos();
                _blockSharedPreferences();
                if(context.mounted) {
                  _showSnackBar(context, "Pronostics enregistrés");
                  Navigator.of(context).pop();
                }
              });
            }
          },
          icon: const Icon(Icons.send),
        )
      ],
    );
  }

  void _blockSharedPreferences() {
    var notifier =
        Provider.of<SharedPreferencesNotifier>(context, listen: false);
    notifier.setBlocage(true);
  }

  Future<void> _savePronos() async {
    if (widget.numberVote == 15) {
      await _pronosService.pushPronos(
          Pronos(premierTour: selectedMiss.map((e) => e.nom!).toList()));
    } else if (widget.numberVote == 5) {
      await _pronosService.pushPronos(
          Pronos(deuxiemeTour: selectedMiss.map((e) => e.nom!).toList()));
    }
    await _clearSelectedCandidates();
  }

  Future<void> _clearSelectedCandidates() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('selectedCandidates');
  }

  void _showSnackBar(BuildContext context, String message) {
    ScaffoldMessenger.of(context).clearSnackBars();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Row(
          children: [
            const Icon(Icons.check),
            const SizedBox(width: 10.0),
            Text(
              message,
              style: TextStyle(color: Colors.grey[800]),
            ),
          ],
        ),
        duration: const Duration(seconds: 1),
        backgroundColor: Colors.green[200],
        showCloseIcon: true,
        closeIconColor: Colors.grey[800],
        padding: const EdgeInsets.all(10),
        behavior: SnackBarBehavior.floating,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
      ),
    );
  }

  Future<void> _showConfirmationDialog(
      BuildContext context, Function() actionCallback) async {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Confirmation'),
          content: const Text('Êtes-vous sûr de valider vos pronos ?'),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text('Annuler'),
            ),
            TextButton(
              onPressed: () {
                actionCallback();
                Navigator.of(context).pop();
              },
              child: const Text('Confirmer'),
            ),
          ],
        );
      },
    );
  }
}
