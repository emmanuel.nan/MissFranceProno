import 'dart:async';

import 'package:flutter/material.dart';
import 'package:missfranceprono/service/resultat_service.dart';
import 'package:provider/provider.dart';

import 'mes_shared_preferences.dart';


class ClassementPage extends StatefulWidget {
  const ClassementPage({super.key});

  @override
  State<ClassementPage> createState() => _ClassementPageState();
}

class _ClassementPageState extends State<ClassementPage> {
  final _classementService = ResultatRepository();
  late SharedPreferencesNotifier notifier;

  Future<List> _getClassement() async {
    var response = await _classementService.getClassement();

    return response.docs.map((resultat) {
      var classement = (resultat.data() as Map<String, dynamic>);
      return {
        "displayName": classement["displayName"],
        "points": classement["points"]
      };
    }).toList();
  }

  void _updateData() {
    setState(() {
    });
  }

  @override
  void initState() {
    notifier = Provider.of<SharedPreferencesNotifier>(context, listen: false);
    super.initState();

    notifier.addListener(_updateData);
  }

  @override
  void dispose() {
    notifier.removeListener(_updateData);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _getClassement(),
      builder: (context, snapshot) {
        return _buildResultWidget(snapshot);
      },
    );
  }

  Widget _buildResultWidget(AsyncSnapshot snapshot) {
    Widget result = Container();

    switch (snapshot.connectionState) {
      case ConnectionState.waiting:
        result = _buildLoadingWidget();
        break;
      case ConnectionState.done:
        if (snapshot.hasData) {
          result = _buildClassementWidget(snapshot.data);
        } else {
          result = _buildNoDataWidget();
        }
        break;
      default:
        result = _buildErrorWidget(snapshot);
    }

    return Container(
      child: result,
    );
  }

  Widget _buildLoadingWidget() {
    return const Center(
      child: SizedBox(
        width: 60,
        height: 60,
        child: CircularProgressIndicator(),
      ),
    );
  }

  Widget _buildClassementWidget(List<Map<String, dynamic>> resultats) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _buildClassementTitle(),
        _buildCandidatsList(resultats),
      ],
    );
  }

  Widget _buildClassementTitle() {
    return const Center(
      child: Padding(
        padding: EdgeInsets.all(8.0),
        child: Text(
          "Classement",
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }

  Widget _buildCandidatsList(List<Map<String, dynamic>> resultats) {
    return Expanded(
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: resultats.length + 1,
        itemBuilder: (BuildContext context, int index) {
          return _buildCandidatRow(index, resultats);
        },
      ),
    );
  }

  Widget _buildCandidatRow(int index, List<Map<String, dynamic>> resultats) {
    if (index == resultats.length) {
      return Container();
    }

    var candidate = resultats[index];

    return Row(
      children: [
        _buildColonneNumero(index),
        _buildColonneNomPoints(candidate),
      ],
    );
  }

  Widget _buildColonneNumero(int index) {
    return Container(
      padding: const EdgeInsets.all(16.0),
      width: 50,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '${index + 1}',
            style: const TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildColonneNomPoints(Map<String, dynamic> candidate) {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('${candidate["displayName"]}'),
          Text('${candidate["points"]} points'),
        ],
      ),
    );
  }

  Widget _buildNoDataWidget() {
    return const Center(
      child: Text(
        "Aucun utilisateur inscrit",
        style: TextStyle(fontSize: 16),
      ),
    );
  }

  Widget _buildErrorWidget(AsyncSnapshot snapshot) {
    return Center(
      child: Text(
        "Un problème quelque part... ${snapshot.connectionState}",
        style: const TextStyle(fontSize: 16),
      ),
    );
  }
}
