import 'package:json_annotation/json_annotation.dart';


part 'pronos.g.dart';

@JsonSerializable()
class Pronos {
  String? utilisateur;
  String? displayName;

  List<String>? premierTour;
  List<String>? deuxiemeTour;
  String? troisiemeDauphine;
  String? deuxiemeDauphine;
  String? missFrance;

  Pronos({
    this.utilisateur,
    this.displayName,
    this.premierTour,
    this.deuxiemeTour,
    this.troisiemeDauphine,
    this.deuxiemeDauphine,
    this.missFrance
  });

  factory Pronos.fromJson(Map<String, dynamic> json) {
    return _$PronosFromJson(json);
  }

  Map<String, dynamic> toJson() => _$PronosToJson(this);
}
