import 'package:json_annotation/json_annotation.dart';

part 'candidate.g.dart';

@JsonSerializable()
class Candidate {
  String? region;
  String? nom;
  String? age;
  String? taille;
  String? elue;
  List<String>? url;
  bool? estGagnantePremierTour;
  bool? estGagnanteDeuxiemeTour;
  bool? estTroisiemeDauphine;
  bool? estDeuxiemeDauphine;
  bool? estMissFrance;
  bool? estEliminee;

  Candidate({
    this.region,
    this.nom,
    this.age,
    this.taille,
    this.elue,
    this.url,
    this.estGagnantePremierTour,
    this.estGagnanteDeuxiemeTour,
    this.estTroisiemeDauphine,
    this.estDeuxiemeDauphine,
    this.estMissFrance,
    this.estEliminee,
  });

  factory Candidate.fromJson(Map<String, dynamic> json) {
    return _$CandidateFromJson(json);
  }

  Map<String, dynamic> toJson() => _$CandidateToJson(this);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Candidate &&
          nom == other.nom &&
          region == other.region;

  @override
  int get hashCode => nom.hashCode ^ region.hashCode;
}
