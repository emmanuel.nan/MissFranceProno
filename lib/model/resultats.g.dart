// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'resultats.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Resultats _$ResultatsFromJson(Map<String, dynamic> json) => Resultats(
      utilisateur: json['utilisateur'] as String?,
      nbGoodResultPremierTour: json['nbGoodResultPremierTour'] as int?,
      nbGoodResultSecondTour: json['nbGoodResultSecondTour'] as int?,
      troisiemeDauphine: json['troisiemeDauphine'] as bool?,
      deuxiemeDauphine: json['deuxiemeDauphine'] as bool?,
      missFrance: json['missFrance'] as bool?,
      points: json['points'] as int?,
    );

Map<String, dynamic> _$ResultatsToJson(Resultats instance) => <String, dynamic>{
      'utilisateur': instance.utilisateur,
      'nbGoodResultPremierTour': instance.nbGoodResultPremierTour,
      'nbGoodResultSecondTour': instance.nbGoodResultSecondTour,
      'troisiemeDauphine': instance.troisiemeDauphine,
      'deuxiemeDauphine': instance.deuxiemeDauphine,
      'missFrance': instance.missFrance,
      'points': instance.points,
    };
