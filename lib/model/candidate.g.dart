// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'candidate.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Candidate _$CandidateFromJson(Map<String, dynamic> json) => Candidate(
      region: json['region'] as String?,
      nom: json['nom'] as String?,
      age: json['age'] as String?,
      taille: json['taille'] as String?,
      elue: json['elue'] as String?,
      url: (json['url'] as List<dynamic>?)?.map((e) => e as String).toList(),
      estGagnantePremierTour: json['estGagnantePremierTour'] as bool?,
      estGagnanteDeuxiemeTour: json['estGagnanteDeuxiemeTour'] as bool?,
      estTroisiemeDauphine: json['estTroisiemeDauphine'] as bool?,
      estDeuxiemeDauphine: json['estDeuxiemeDauphine'] as bool?,
      estMissFrance: json['estMissFrance'] as bool?,
      estEliminee: json['estEliminee'] as bool?,
    );

Map<String, dynamic> _$CandidateToJson(Candidate instance) => <String, dynamic>{
      'region': instance.region,
      'nom': instance.nom,
      'age': instance.age,
      'taille': instance.taille,
      'elue': instance.elue,
      'url': instance.url,
      'estGagnantePremierTour': instance.estGagnantePremierTour,
      'estGagnanteDeuxiemeTour': instance.estGagnanteDeuxiemeTour,
      'estTroisiemeDauphine': instance.estTroisiemeDauphine,
      'estDeuxiemeDauphine': instance.estDeuxiemeDauphine,
      'estMissFrance': instance.estMissFrance,
      'estEliminee': instance.estEliminee,
    };
