import 'package:json_annotation/json_annotation.dart';

part 'resultats.g.dart';

@JsonSerializable()
class Resultats {
  String? utilisateur;

  int? nbGoodResultPremierTour;
  int? nbGoodResultSecondTour;
  bool? troisiemeDauphine;
  bool? deuxiemeDauphine;
  bool? missFrance;
  int? points;

  Resultats({
    this.utilisateur,
    this.nbGoodResultPremierTour,
    this.nbGoodResultSecondTour,
    this.troisiemeDauphine,
    this.deuxiemeDauphine,
    this.missFrance,
    this.points
  });

  factory Resultats.fromJson(Map<String, dynamic> json) {
    return _$ResultatsFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ResultatsToJson(this);
}
