// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pronos.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Pronos _$PronosFromJson(Map<String, dynamic> json) => Pronos(
      utilisateur: json['utilisateur'] as String?,
      displayName: json['displayName'] as String?,
      premierTour: (json['premierTour'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      deuxiemeTour: (json['deuxiemeTour'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      troisiemeDauphine: json['troisiemeDauphine'] as String?,
      deuxiemeDauphine: json['deuxiemeDauphine'] as String?,
      missFrance: json['missFrance'] as String?,
    );

Map<String, dynamic> _$PronosToJson(Pronos instance) => <String, dynamic>{
      'utilisateur': instance.utilisateur,
      'displayName': instance.displayName,
      'premierTour': instance.premierTour,
      'deuxiemeTour': instance.deuxiemeTour,
      'troisiemeDauphine': instance.troisiemeDauphine,
      'deuxiemeDauphine': instance.deuxiemeDauphine,
      'missFrance': instance.missFrance,
    };
