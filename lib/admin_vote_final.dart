
import 'package:flutter/material.dart';
import 'package:missfranceprono/service/admin_service.dart';

import 'candidate.dart';
import 'model/candidate.dart';
import 'service/candidate_service.dart';

class AdminVoteFinalPage extends StatefulWidget {
  const AdminVoteFinalPage({super.key, required this.title});
  final String title;

  @override
  State<AdminVoteFinalPage> createState() => _AdminVoteFinalPageState();
}

class _AdminVoteFinalPageState extends State<AdminVoteFinalPage> {
  final _candidateService = CandidateRepository();
  final _adminService = AdminRepository();
  var _candidates = List<Candidate>.empty();

  void _initalizeCandidates() async {
    var response = await _candidateService.getCandidates(false);

    setState(() {
      _candidates = response.docs
          .map((doc) => Candidate.fromJson(doc.data() as Map<String, dynamic>))
          .toList();
    });
  }

  @override
  void initState() {
    super.initState();
    _initalizeCandidates();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            _buildCandidatesHeader(),
            Expanded(child: _buildReorderableListView()),
            _buildSelectionContainer(),
          ],
        ),
      ),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      title: const Text("Miss France Prono"),
      elevation: 0,
      foregroundColor: Colors.grey[800],
      backgroundColor: Colors.white,
    );
  }

  Widget _buildCandidatesHeader() {
    return Column(
      children: [
        _buildHeaderText("Candidates finalistes :"),
        _buildHeaderText("Trier votre sélection dans l'ordre :",
            isItalic: true),
      ],
    );
  }

  Widget _buildHeaderText(String text, {bool isItalic = false, Color? textColor }) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 5, 10, 2),
      child: Text(
        text,
        style: TextStyle(
          color: textColor ?? Colors.grey[800],
          fontWeight: FontWeight.bold,
          fontSize: isItalic ? 14 : 20,
          fontStyle: isItalic ? FontStyle.italic : FontStyle.normal,
        ),
      ),
    );
  }

  Widget _buildReorderableListView() {
    return ReorderableListView.builder(
      key: UniqueKey(),
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      padding: const EdgeInsets.symmetric(horizontal: 0),
      itemCount: _candidates.length,
      itemBuilder: (context, index) {
        return _buildReorderableListItem(index);
      },
      onReorder: _onReorder,
    );
  }

  Widget _buildReorderableListItem(int index) {
    var candidate = _candidates[index];
    var name = candidate.nom!;
    var region = candidate.region!;
    var url = candidate.url![0];

    return ReorderableDragStartListener(
      index: index,
      key: ValueKey(index),
      child: ListTile(
        leading: _buildHeroAvatar(name, url),
        title: Text(
          name,
          style: const TextStyle(
            fontWeight: FontWeight.w500,
          ),
        ),
        subtitle: Text(region),
        trailing: const Icon(
          Icons.reorder,
          color: Colors.grey,
        ),
        onTap: () => _navigateToCandidatePage(candidate),
      ),
    );
  }

  Widget _buildHeroAvatar(String name, String url) {
    return Hero(
      tag: name,
      child: CircleAvatar(
        radius: 35,
        backgroundImage: NetworkImage(url),
      ),
    );
  }

  void _onReorder(int oldIndex, int newIndex) {
    setState(() {
      if (oldIndex < newIndex) {
        newIndex -= 1;
      }
      final dynamic item = _candidates.removeAt(oldIndex);
      _candidates.insert(newIndex, item);
    });
  }

  Widget _buildSelectionContainer() {
    return Container(
      margin: const EdgeInsets.only(top: 50),
      decoration: const BoxDecoration(
        color: Color.fromARGB(175, 99, 21, 86),
        borderRadius: BorderRadius.vertical(top: Radius.circular(25)),
      ),
      height: 150,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildHeaderText("Sélection :",
                isItalic: false, textColor: Colors.white),
            _buildSelectedCandidateText("Miss France", 0),
            _buildSelectedCandidateText("Deuxième dauphine", 1),
            _buildSelectedCandidateText("Troisième dauphine", 2),
            _buildValiderButton(),
          ],
        ),
      ),
    );
  }

  Widget _buildSelectedCandidateText(String title, int index) {
    return Text(
      "$title : ${_candidates.isNotEmpty ? _candidates[index].nom! : " - "}",
      style: const TextStyle(color: Colors.white),
    );
  }

  Widget _buildValiderButton() {
    return ElevatedButton(
      onPressed: () => _validerTroisiemeEtape(),
      child: const Center(
        child: Text('Valider'),
      ),
    );
  }

  void _validerTroisiemeEtape() async {
    await _adminService.validerTroisiemeEtape(
        _candidates[0], _candidates[1], _candidates[2]);

    if(context.mounted) {
      ScaffoldMessenger.of(context).clearSnackBars();
      Navigator.of(context).pop();

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: const Text("Votre pronostique est enregistré"),
        duration: const Duration(seconds: 1),
        backgroundColor: Colors.green,
        width: 300.0,
        padding: const EdgeInsets.all(10),
        behavior: SnackBarBehavior.floating,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
      ));
    }
  }

  void _navigateToCandidatePage(Candidate candidate) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => CandidatePage(candidate: candidate,),
      ),
    );
  }
}
