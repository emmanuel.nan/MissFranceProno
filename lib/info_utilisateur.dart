import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:missfranceprono/service/resultat_service.dart';

import 'service/auth_service.dart';

class InfoUtilisateurPage extends StatefulWidget {
  const InfoUtilisateurPage({super.key});

  @override
  State<InfoUtilisateurPage> createState() => _InfoUtilisateurPageState();
}

class _InfoUtilisateurPageState extends State<InfoUtilisateurPage> {
  final _authService = AuthRepository();
  final _resultatService = ResultatRepository();
  late User? _currentUser;

  final pseudoCtrl = TextEditingController();

  @override
  void initState() {
    super.initState();
    _currentUser = _authService.getCurrentUser();
    pseudoCtrl.text = _currentUser!.displayName ?? '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(context),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: _buildBody(context),
      ),
    );
  }

  AppBar _buildAppBar(BuildContext context) {
    return AppBar(
      scrolledUnderElevation: 0.0,
      elevation: 0,
      backgroundColor: Colors.transparent,
      leading: IconButton(
        onPressed: () {
          context.pop();
        },
        icon: const Icon(
          Icons.arrow_back_ios,
          size: 20,
          color: Colors.black,
        ),
      ),
      systemOverlayStyle: SystemUiOverlayStyle.dark,
    );
  }

  Widget _buildBody(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height - 100,
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildHeader(),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 40),
            child: Column(
              children: [
                _buildInputField(
                    label: "Pseudo",
                    controller: pseudoCtrl,
                    onSubmitted: (value) {
                      _handleUpdatePseudo(context);
                    }),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 40),
            child: FilledButton(
              style: FilledButton.styleFrom(
                minimumSize: const Size.fromHeight(50),
              ),
              onPressed: () => _handleUpdatePseudo(context),
              child: const Text('Modifier'),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 40),
            child: FilledButton(
              style: FilledButton.styleFrom(
                minimumSize: const Size.fromHeight(50),
              ),
              onPressed: () async => await _authService
                  .signOut()
                  .then((value) => context.go('/signin')),
              child: const Text('Se déconnecter'),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildHeader() {
    return Column(
      children: [
        const Icon(Icons.account_circle, size: 80),
        const SizedBox(height: 20),
         Text(
           "Salut ${_currentUser!.displayName}",
          style: const TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(height: 20),
        Text(
         "Modification du pseudo",
          style: TextStyle(
            fontSize: 15,
            color: Colors.grey[700],
          ),
        ),
      ],
    );
  }

  Widget _buildInputField({
    required String label,
    required TextEditingController controller,
    bool obscureText = false,
    TextInputAction textInputAction = TextInputAction.next,
    required Function(String) onSubmitted,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: const TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.w400,
            color: Colors.black87,
          ),
        ),
        const SizedBox(height: 5),
        TextField(
          controller: controller,
          textInputAction: textInputAction,
          obscureText: obscureText,
          onSubmitted: onSubmitted,
          decoration: const InputDecoration(
            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color.fromARGB(255, 189, 189, 189),
              ),
            ),
            border: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color.fromARGB(255, 189, 189, 189),
              ),
            ),
          ),
        ),
        const SizedBox(height: 30),
      ],
    );
  }

  void _handleUpdatePseudo(BuildContext context) async {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return const Dialog(
          elevation: 0,
          backgroundColor: Colors.transparent,
          child: Center(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                CircularProgressIndicator(),
              ],
            ),
          ),
        );
      },
    );

    try {
      await _currentUser!.updateDisplayName(pseudoCtrl.text);
      await _resultatService.initResultat(_currentUser!.email!, pseudoCtrl.text).then((value) {
        Navigator.pop(context);
        ScaffoldMessenger.of(context).clearSnackBars();
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Row(
            children: [
              const Icon(Icons.check),
              const SizedBox(width: 10.0),
              Text(
                "Vous êtes désormais ${_authService.getCurrentUser()?.displayName}",
                style: TextStyle(color: Colors.grey[800]),
              )
            ],
          ),
          duration: const Duration(seconds: 2),
          backgroundColor: Colors.green[200],
          showCloseIcon: true,
          closeIconColor: Colors.grey[800],
          padding: const EdgeInsets.all(10),
          behavior: SnackBarBehavior.floating,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
        ));
      });
    } on FirebaseAuthException catch (e) {
      if (context.mounted) {
        Navigator.pop(context);
        showError(context, errorMessage: e.message);
      }
    }
  }

  void showError(BuildContext context,
      {String? errorMessage = "Erreur de connexion"}) {
    ScaffoldMessenger.of(context).clearSnackBars();
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Row(
        children: [
          const Icon(Icons.warning),
          const SizedBox(width: 10.0),
          Flexible(
            child: Text(
              errorMessage!,
              style: TextStyle(color: Colors.grey[800]),
            ),
          ),
        ],
      ),
      duration: const Duration(seconds: 3),
      backgroundColor: Colors.red[200],
      showCloseIcon: true,
      closeIconColor: Colors.grey[800],
      padding: const EdgeInsets.all(10),
      behavior: SnackBarBehavior.floating,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
    ));
  }
}
