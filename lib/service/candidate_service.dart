import 'package:cloud_firestore/cloud_firestore.dart';


class CandidateRepository {
  final CollectionReference candidateCollection =
      FirebaseFirestore.instance.collection('candidate');

  Future<QuerySnapshot> getCandidates(bool estEliminee) async {
    return await candidateCollection.where("estEliminee", isEqualTo: estEliminee).get();
  }

  Future<QuerySnapshot> getCandidate(String name) async {
    return await candidateCollection.where("nom", isEqualTo: name).get();
  }
}
