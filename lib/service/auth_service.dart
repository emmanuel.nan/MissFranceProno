import 'package:firebase_auth/firebase_auth.dart';

class AuthRepository {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<void> signUp(String userMail, String password, String pseudo) async {
    try {
      UserCredential userCredential = await _auth.createUserWithEmailAndPassword(
        email: userMail,
        password: password,
      );

      await userCredential.user?.updateDisplayName(pseudo);
      await userCredential.user?.sendEmailVerification();
    } on FirebaseAuthException catch (e) {
      print("Error during sign up: $e");
      throw e;
    }
  }

  Future<UserCredential> signIn(String userMail, String password) async {
    try {
      UserCredential userCredential = await _auth.signInWithEmailAndPassword(
        email: userMail,
        password: password,
      );

      if (userCredential.user!.emailVerified) {
        return userCredential;
      } else {
        await signOut();
        throw FirebaseAuthException(
          code: 'email_not_verified',
          message: 'Email not verified. Please check your email for verification.',
        );
      }
    } on FirebaseAuthException catch (e) {
      print("Error during sign in: $e");
      throw e;
    }
  }

  Future<void> signOut() async {
    await _auth.signOut();
  }

  User? getCurrentUser() {
    return _auth.currentUser;
  }
}
