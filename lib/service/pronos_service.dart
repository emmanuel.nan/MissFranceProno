import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:missfranceprono/model/pronos.dart';

class PronosRepository {
  final CollectionReference<Object?> pronosCollection =
      FirebaseFirestore.instance.collection('pronos');
  final FirebaseAuth _authFirebase = FirebaseAuth.instance;

  Future<DocumentSnapshot<Object?>> getPronos() async {
    try {
      User firebaseUser = _authFirebase.currentUser!;

      return await pronosCollection.doc(firebaseUser.email).get();
    } catch (e) {
      print("Error while fetching pronos: $e");
      rethrow;
    }
  }

  Future<void> pushPronos(Pronos pronos) async {
    try {
      User firebaseUser = _authFirebase.currentUser!;
      final useremail = firebaseUser.email;

      pronos.utilisateur = useremail;
      pronos.displayName = firebaseUser.displayName;

      await pronosCollection
          .doc(useremail)
          .set(pronos.toJson(), SetOptions(merge: true));
    } catch (e) {
      print("Failed to push pronos: $e");
      rethrow;
    }
  }
}
