import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:missfranceprono/model/pronos.dart';
import 'package:missfranceprono/model/resultats.dart';

import '../model/candidate.dart';

class AdminRepository {
  final CollectionReference candidateDb =
      FirebaseFirestore.instance.collection('candidate');
  final CollectionReference pronosDb =
      FirebaseFirestore.instance.collection('pronos');
  final CollectionReference resultatsDb =
      FirebaseFirestore.instance.collection('resultats');
  final CollectionReference concoursDb =
      FirebaseFirestore.instance.collection('concours');

  Future<void> initCandidates() async {
    await _initializeResultats();
    await _clearPronos();
    await _resetCandidates();
    await _resetConcours();
  }

  Future<void> validerPremiereEtape(List<Candidate> candidates) async {
    await _updateMissStatus(candidates, "estGagnantePremierTour");
    await _calculateResults(candidates, "premierTour");
    await _changeMode(1);
  }

  Future<void> validerDeuxiemeEtape(List<Candidate> candidates) async {
    await _updateMissStatus(candidates, "estGagnanteDeuxiemeTour");
    await _calculateResults(candidates, "deuxiemeTour");
    await _changeMode(2);
  }

  Future<void> validerTroisiemeEtape(
    Candidate missFrance,
    Candidate deuxiemeDauphine,
    Candidate troisiemeDauphine,
  ) async {
    await _updateFinalResults(missFrance, deuxiemeDauphine, troisiemeDauphine);
    await _calculateFinalResults(missFrance, deuxiemeDauphine, troisiemeDauphine);
    await _changeMode(3);
  }

  Future<void> _initializeResultats() async {
    await resultatsDb.get().then((value) => value.docs.forEach((element) {
          resultatsDb.doc(element.id).update({
            "displayName": element["displayName"],
            "points": 0,
          });
        }));
  }

  Future<void> _clearPronos() async {
    await pronosDb.get().then((value) =>
        value.docs.forEach((element) => element.reference.delete()));
  }

  Future<void> _resetCandidates() async {
    await candidateDb.get().then((value) {
      value.docs.forEach((doc) {
        candidateDb.doc(doc.id).set({
          "estDeuxiemeDauphine": false,
          "estGagnantePremierTour": false,
          "estGagnanteDeuxiemeTour": false,
          "estTroisiemeDauphine": false,
          "estMissFrance": false,
          "estEliminee": false,
        }, SetOptions(merge: true));
      });
    });
  }

  Future<void> _resetConcours() async {
    await concoursDb
        .doc("missfrance2023")
        .update({"mode": -1, "blocage": false});
    await concoursDb
        .doc("missfrance2023")
        .update({"mode": 0, "blocage": false});
  }

  Future<void> _updateMissStatus(
      List<Candidate> candidates, String statusField) async {
    await candidateDb.get().then((value) {
      value.docs.forEach((doc) async {
        if (candidates.any((cand) => cand.nom == doc.get("nom"))) {
          await candidateDb.doc(doc.id).update({statusField: true});
        } else {
          await candidateDb.doc(doc.id).update({"estEliminee": true});
        }
      });
    });
  }

  Future<void> _calculateResults(List<Candidate> candidates, String tour) async {
  await pronosDb.get().then((value) {
    value.docs.forEach((pronosDoc) async {
      var prono = Pronos.fromJson(
          pronosDoc.data() as Map<String, dynamic>? ?? {});

      // Utilisez les champs spécifiques pour chaque tour
      var identiques = (tour == 'premierTour')
          ? prono.premierTour?.where((element) =>
              candidates.any((candidate) => element == candidate.nom!)).toList() ?? []
          : (tour == 'deuxiemeTour')
              ? prono.deuxiemeTour?.where((element) =>
                  candidates.any((candidate) => element == candidate.nom!)).toList() ?? []
              : [];

      var points = identiques.length * 10;
      var nbGoodResult = identiques.length;

      await _updateResultats(
          prono.utilisateur!, prono.displayName!, points, nbGoodResult, tour == 'deuxiemeTour' ? 'secondTour' : tour);
    });
  });
}

  Future<void> _updateResultats(String utilisateur, String displayName,
      int points, int nbGoodResult, String tour) async {
    await resultatsDb.doc(utilisateur).set({
      "displayName": displayName,
      "points": points,
      "nbGoodResult${tour.capitalize()}": nbGoodResult,
    }, SetOptions(merge: true));
  }

  Future<void> _changeMode(int mode) async {
    await Future.delayed(Duration(seconds: 3));
    await concoursDb.doc("missfrance2023").update({"mode": mode, "blocage": false});
  }

  Future<void> _updateFinalResults(
      Candidate missFrance, Candidate deuxiemeDauphine, Candidate troisiemeDauphine) async {
    await _updateMissStatus([missFrance], "estMissFrance");
    await _updateMissStatus([deuxiemeDauphine], "estDeuxiemeDauphine");
    await _updateMissStatus([troisiemeDauphine], "estTroisiemeDauphine");
  }

  Future<void> _calculateFinalResults(
      Candidate missFrance, Candidate deuxiemeDauphine, Candidate troisiemeDauphine) async {
    await pronosDb.get().then((value) {
      value.docs.forEach((pronosDoc) async {
        var prono = Pronos.fromJson(
            pronosDoc.data() as Map<String, dynamic>? ?? {});
        var points = 0;
        var winMissFrance = prono.missFrance == missFrance.nom;
        var winDeuxiemeDauphine =
            prono.deuxiemeDauphine == deuxiemeDauphine.nom;
        var winTroisiemeDauphine =
            prono.troisiemeDauphine == troisiemeDauphine.nom;

        var resultDoc = await resultatsDb.doc(prono.utilisateur).get();
        if (resultDoc.exists) {
          var result =
              Resultats.fromJson(resultDoc.data() as Map<String, dynamic>);
          points = (result.points ?? 0);
        }

        if (winMissFrance) points += 30;
        if (winDeuxiemeDauphine) points += 20;
        if (winTroisiemeDauphine) points += 20;

        await resultatsDb.doc(prono.utilisateur).set({
          "displayName": prono.displayName,
          "troisiemeDauphine": winTroisiemeDauphine,
          "deuxiemeDauphine": winDeuxiemeDauphine,
          "missFrance": winMissFrance,
          "points": points,
        }, SetOptions(merge: true));
      });
    });
  }
}

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}
