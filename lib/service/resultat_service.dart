import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class ResultatRepository {
  final CollectionReference<Object?> resultatsCollection =
      FirebaseFirestore.instance.collection('resultats');
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<DocumentSnapshot<Object?>> getResultat() async {
    try {
      User firebaseUser = _auth.currentUser!;

      return await resultatsCollection.doc(firebaseUser.email).get();
    } catch (e) {
      print("Error while fetching result: $e");
      rethrow;
    }
  }

  Future<QuerySnapshot> getClassement() async {
    try {
      return await resultatsCollection.orderBy("points", descending: true).get();
    } catch (e) {
      print("Error while fetching ranking: $e");
      rethrow;
    }
  }

  Future<void> initResultat(String email, String displayName) async {
    try {
      var document = await resultatsCollection.doc(email).get();

      if (!document.exists) {
        // Le document n'existe pas, on peut le créer
        await resultatsCollection.doc(email).set(
            {"displayName": displayName, "points": 0},
            SetOptions(merge: false));
      }
    } catch (e) {
      print("Error while initializing result: $e");
      rethrow;
    }
  }
}