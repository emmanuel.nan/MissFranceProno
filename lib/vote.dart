import 'package:flutter/material.dart';
import 'package:missfranceprono/service/candidate_service.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

import 'candidate.dart';
import 'model/candidate.dart';

class VotePage extends StatefulWidget {
  const VotePage({super.key, required this.title, required this.numberVote});
  final String title;
  final int numberVote;

  @override
  State<VotePage> createState() => _VotePageState();
}

class _VotePageState extends State<VotePage> {
  var _scrollController = ScrollController();
  var candidateService = CandidateRepository();
  var selectedMiss = List.empty(growable: true);
  var candidates = List<Candidate>.empty(growable: true);

  void getMiss() async {
    var response = await candidateService.getCandidates(false);

    setState(() {
      candidates = response.docs
          .map((doc) => Candidate.fromJson(doc.data() as Map<String, dynamic>))
          .toList();
    });
  }

  @override
  void initState() {
    super.initState();
    
    _scrollController = ScrollController();
    loadSelectedCandidates();
    getMiss();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("${selectedMiss.length}/${widget.numberVote}"),
          elevation: 0,
          foregroundColor: Colors.grey[800],
          backgroundColor: Colors.white,
          actions: [
            IconButton(
              icon: const Icon(Icons.check),
              onPressed: () {
                ScaffoldMessenger.of(context).clearSnackBars();
                Navigator.of(context).pop();
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Row(
                    children: [
                      const Icon(Icons.check),
                      const SizedBox(
                        width: 10.0,
                      ),
                      Text(
                        "Pronostique enregistré",
                        style: TextStyle(color: Colors.grey[800]),
                      )
                    ],
                  ),
                  duration: const Duration(seconds: 1),
                  backgroundColor: Colors.green[200],
                  showCloseIcon: true,
                  closeIconColor: Colors.grey[800],
                  padding: const EdgeInsets.all(10),
                  behavior: SnackBarBehavior.floating,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ));
              },
            ),
          ],
        ),
        body: ListView.builder(
            controller: _scrollController,
            itemCount: candidates.length + 1, // +1 for the additional item
            itemBuilder: (BuildContext context, int index) {
              if (index == candidates.length) {
                // Ajoutez ici un élément supplémentaire si nécessaire
                return Container();
              }

              var candidate = candidates[index];

              return MissFranceItem(
                candidate,
              );
            }));
  }

  // ignore: non_constant_identifier_names
  Widget MissFranceItem(Candidate candidate) {
    final bool isSelected = selectedMiss.contains(candidate);

    final Widget _leading = isSelected
        ? SizedBox(
            height: 40.0,
            width: 40.0,
            child: IconButton(
              icon: const Icon(Icons.check),
              onPressed: (selectedMiss.length >= widget.numberVote &&
                      !selectedMiss.contains(candidate))
                  ? null
                  : () {},
              style: enabledFilledButtonStyle(
                  selectedMiss.contains(candidate),
                  Theme.of(context).colorScheme),
            ),
          )
        : CircleAvatar(
            radius: 35,
            backgroundImage: NetworkImage(candidate.url![0]),
          );

    return Padding(
      padding: const EdgeInsets.only(left: 10.0, right: 10.0, bottom: 5.0),
      child: ListTile(
        contentPadding: const EdgeInsets.only(left: 5.0, right: 5.0),
        splashColor: Colors.pink[50],
        selectedColor: Colors.white,
        selectedTileColor: Colors.pink[100],
        selected: isSelected,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        leading: GestureDetector(
          child: SizedBox(
            width: 70.0,
            child: AnimatedSwitcher(
              duration: const Duration(milliseconds: 500),
              transitionBuilder: (Widget child, Animation<double> animation) {
                return ScaleTransition(
                    scale: animation,
                    alignment: Alignment.center,
                    filterQuality: FilterQuality.medium,
                    child: child);
              },
              child: _leading,
            ),
          ),
        ),
        title: Text(
          candidate.nom ?? '',
          style: const TextStyle(
            fontWeight: FontWeight.w500,
          ),
        ),
        subtitle: Text(candidate.region ?? ''),
        onLongPress: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => CandidatePage(candidate: candidate),
            ),
          );
        },
        onTap: () {
          if (!selectedMiss.contains(candidate)) {
            if (selectedMiss.length < widget.numberVote) {
              setState(() {
                selectedMiss.add(candidate);
              });
            } else {
              ScaffoldMessenger.of(context).clearSnackBars();
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Row(
                  children: [
                    const Icon(Icons.warning_amber_rounded),
                    const SizedBox(
                      width: 10.0,
                    ),
                    Text(
                      "Vous avez déjà sélectionné ${widget.numberVote} miss",
                      style: TextStyle(color: Colors.grey[800]),
                    )
                  ],
                ),
                duration: const Duration(seconds: 1),
                backgroundColor: Colors.amber[200],
                showCloseIcon: true,
                closeIconColor: Colors.grey[800],
                padding: const EdgeInsets.all(10),
                behavior: SnackBarBehavior.floating,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ));
            }
          } else {
            setState(() {
              selectedMiss.remove(candidate);
            });
          }
          saveSelectedCandidates();
        },
      ),
    );
  }

  Future<void> loadSelectedCandidates() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String>? selectedCandidatesJson =
        prefs.getStringList('selectedCandidates');

    if (selectedCandidatesJson != null) {
      setState(() {
        selectedMiss = selectedCandidatesJson
            .map((json) => Candidate.fromJson(jsonDecode(json)))
            .toList();
      });
    }
  }

  Future<void> saveSelectedCandidates() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> selectedCandidatesJson = selectedMiss
        .map((candidate) => jsonEncode(candidate.toJson()))
        .toList();

    prefs.setStringList('selectedCandidates', selectedCandidatesJson);
  }

  ButtonStyle enabledFilledButtonStyle(bool selected, ColorScheme colors) {
    return IconButton.styleFrom(
      foregroundColor: selected ? colors.onPrimary : colors.primary,
      backgroundColor: selected ? colors.primary : colors.surfaceVariant,
      disabledForegroundColor: colors.onSurface.withOpacity(0.38),
      disabledBackgroundColor: colors.onSurface.withOpacity(0.12),
      hoverColor: selected
          ? colors.onPrimary.withOpacity(0.08)
          : colors.primary.withOpacity(0.08),
      focusColor: selected
          ? colors.onPrimary.withOpacity(0.12)
          : colors.primary.withOpacity(0.12),
      highlightColor: selected
          ? colors.onPrimary.withOpacity(0.12)
          : colors.primary.withOpacity(0.12),
    );
  }
}
