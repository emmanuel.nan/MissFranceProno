import 'dart:async';

import 'package:flutter/material.dart';
import 'package:missfranceprono/model/resultats.dart';
import 'package:provider/provider.dart';

import 'mes_shared_preferences.dart';
import 'service/resultat_service.dart';

class MesResultatsPage extends StatefulWidget {
  const MesResultatsPage({super.key});

  @override
  State<MesResultatsPage> createState() => _MesResultatsPageState();
}

class _MesResultatsPageState extends State<MesResultatsPage> {
  final _resultatService = ResultatRepository();
  late SharedPreferencesNotifier notifier;

  int _currentMode = 0;

  Future<Resultats?> getData() async {
    var response = await _resultatService.getResultat();

    return response.exists
        ? Resultats.fromJson(response.data() as Map<String, dynamic>)
        : Resultats();
  }

  void _updateData() {
    setState(() {
      _currentMode = notifier.mode;
    });
  }

  @override
  void initState() {
    notifier = Provider.of<SharedPreferencesNotifier>(context, listen: false);
    super.initState();

    notifier.addListener(_updateData);
  }

  @override
  void dispose() {
    notifier.removeListener(_updateData);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Resultats?>(
      future: getData(),
      builder: (context, snapshot) {
        Widget result = Container();

        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            result = _buildLoadingIndicator();
            break;
          case ConnectionState.done:
            if (snapshot.hasData) {
              result = _buildResultContent(snapshot.data!);
            }
            break;
          default:
            result = _buildErrorIndicator(snapshot.connectionState);
        }

        return Container(
          child: result,
        );
      },
    );
  }

  Widget _buildLoadingIndicator() {
    return const Center(
      child: SizedBox(
        width: 60,
        height: 60,
        child: CircularProgressIndicator(),
      ),
    );
  }

  Widget _buildResultContent(Resultats data) {
    List<Widget> contenu = _buildResultWidgets(data);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: contenu,
    );
  }

  List<Widget> _buildResultWidgets(Resultats data) {
    List<Widget> widgets = List<Widget>.empty(growable: true);

    if (_currentMode == 0) {
      widgets.add(_buildNoResultWidget());
    } else if (_currentMode > 0) {
      widgets.addAll(_buildTourWidgets(data));

      if (_currentMode > 2) {
        widgets.addAll(_buildOtherResultWidgets(data));
      }
    }

    widgets.insert(
      0,
      const Center(
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: Text(
            "Mes résultats",
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );

    return widgets;
  }

  Widget _buildNoResultWidget() {
    return const Expanded(
      child: Center(
        child: Text(
          "Aucun résultat, soit patient !",
          style: TextStyle(fontSize: 16),
        ),
      ),
    );
  }

  List<Widget> _buildTourWidgets(Resultats data) {
    List<Widget> tourWidgets = List<Widget>.empty(growable: true);

    if (_currentMode > 0) {
      tourWidgets.add(_buildTourWidget(
        title: 'Premier tour',
        subtitle:
            '${data.nbGoodResultPremierTour ?? 0} candidates trouvées sur 15',
      ));
    }

    if (_currentMode > 1) {
      tourWidgets.addAll([
        const Divider(),
        _buildTourWidget(
          title: 'Deuxième tour',
          subtitle:
              '${data.nbGoodResultSecondTour ?? 0}  candidates trouvées sur 5',
        ),
      ]);
    }

    return tourWidgets;
  }

  Widget _buildTourWidget({required String title, required String subtitle}) {
    return SizedBox(
      height: 80.0,
      child: Card(
        child: SizedBox.expand(
          child: ListTile(
            leading: const Icon(Icons.album),
            title: Text(title),
            subtitle: Text(subtitle),
          ),
        ),
      ),
    );
  }

  List<Widget> _buildOtherResultWidgets(Resultats data) {
    return [
      const Divider(),
      _buildResultCard('Troisième dauphine', data.troisiemeDauphine),
      const Divider(),
      _buildResultCard('Deuxième dauphine', data.deuxiemeDauphine),
      const Divider(),
      _buildResultCard('Miss France', data.missFrance),
    ];
  }

  Widget _buildResultCard(String title, bool? result) {
    return SizedBox(
      height: 80.0,
      child: Card(
        child: SizedBox.expand(
          child: ListTile(
            leading: const Icon(Icons.album),
            title: Text(title),
            subtitle: result == true
                ? const Text('Trouvée')
                : const Text('Non trouvée'),
          ),
        ),
      ),
    );
  }

  Widget _buildErrorIndicator(ConnectionState connectionState) {
    return Expanded(
      child: Center(
        child: Text("Un problème quelque part... $connectionState"),
      ),
    );
  }
}
