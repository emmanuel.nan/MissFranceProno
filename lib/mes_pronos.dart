import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:missfranceprono/model/candidate.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'candidate.dart';
import 'mes_shared_preferences.dart';

class MesPronosPage extends StatefulWidget {
  const MesPronosPage({super.key});

  @override
  State<MesPronosPage> createState() => _MesPronosPageState();
}

class _MesPronosPageState extends State<MesPronosPage> {
  late SharedPreferencesNotifier notifier;

  int _currentMode = 0;
  bool _isBloque = false;

  Future<List<Candidate>> loadSelectedCandidates() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String>? selectedCandidatesJson =
        prefs.getStringList('selectedCandidates');

    if (selectedCandidatesJson != null) {
      return selectedCandidatesJson
          .map((json) => Candidate.fromJson(jsonDecode(json)))
          .toList();
    }

    return Future(() => List<Candidate>.empty());
  }

  Future<void> saveSelectedCandidates(List<Candidate> candidates) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> selectedCandidatesJson =
        candidates.map((candidate) => jsonEncode(candidate.toJson())).toList();

    prefs.setStringList('selectedCandidates', selectedCandidatesJson);
  }

  void _updateData() {
    setState(() {
      _currentMode = notifier.mode;
      _isBloque = notifier.isBlocage;
    });
  }

  @override
  void initState() {
    notifier = Provider.of<SharedPreferencesNotifier>(context, listen: false);
    super.initState();

    notifier.addListener(_updateData);
  }

  @override
  void dispose() {
    notifier.removeListener(_updateData);
    super.dispose();
  }

  String _buildStatutFinal(int index) {
    String result = "";
    if (_currentMode == 2) {
      if (index == 0) {
        result = ' - Miss france';
      }
      if (index == 1) {
        result = ' - Deuxième dauphine';
      }
      if (index == 2) {
        result = ' - Troisième dauphine';
      }
    }
    return result;
  }

 @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: loadSelectedCandidates(),
      builder: (context, snapshot) {
        Widget result = Container();
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            result = const Center(
              child: SizedBox(
                width: 60,
                height: 60,
                child: CircularProgressIndicator(),
              ),
            );
            break;
          case ConnectionState.done:
            if (snapshot.hasData) {
              var candidates = snapshot.data!;
              result = buildCandidateList(candidates);
            }
            break;
          default:
            result = Expanded(
              child: Center(
                child: Text(
                  "Un problème quelque part... ${snapshot.connectionState}",
                ),
              ),
            );
        }
        return Column(
          children: [
            const Center(
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  "Mes pronos en cours",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            result,
          ],
        );
      },
    );
  }

  Widget buildCandidateList(List<Candidate> candidates) {
    if (candidates.isNotEmpty) {
      return Expanded(
        child: ListView.builder(
          shrinkWrap: true,
          itemCount: candidates.length + 1,
          itemBuilder: (BuildContext context, int index) {
            if (index == candidates.length) {
              return Container();
            }

            var candidate = candidates[index];

            return buildDismissibleCandidate(candidate, index, candidates);
          },
        ),
      );
    } else {
      return const Expanded(
        child: Center(
          child: Text(
            "Aucune miss sélectionnée",
            style: TextStyle(fontSize: 16),
          ),
        ),
      );
    }
  }

  Widget buildDismissibleCandidate(
    Candidate candidate,
    int index,
    List<Candidate> candidates,
  ) {
    return Dismissible(
      key: Key(candidate.nom!),
      direction: _isBloque ? DismissDirection.none : DismissDirection.endToStart,
      onDismissed: (direction) {
        setState(() {
          candidates.removeAt(index);
        });
        saveSelectedCandidates(candidates);

        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: const Text("Prono supprimée"),
            action: SnackBarAction(
              label: "Annuler",
              onPressed: () {
                setState(() {
                  candidates.insert(index, candidate);
                });
                saveSelectedCandidates(candidates);
              },
            ),
          ),
        );
      },
      background: Container(
        color: Colors.red,
        alignment: Alignment.centerRight,
        padding: const EdgeInsets.only(right: 16.0),
        child: const Icon(Icons.delete, color: Colors.white),
      ),
      child: buildCandidateItem(candidate, index),
    );
  }

  Widget buildCandidateItem(Candidate candidate, int index) {
    final Widget leadingWidget = CircleAvatar(
      radius: 35,
      backgroundImage: NetworkImage(candidate.url![0]),
    );

    return Padding(
      padding: const EdgeInsets.only(left: 10.0, right: 10.0, bottom: 5.0),
      child: ListTile(
        contentPadding: const EdgeInsets.only(left: 5.0, right: 5.0),
        splashColor: Colors.pink[50],
        selectedColor: Colors.white,
        selectedTileColor: Colors.pink[100],
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
        ),
        leading: GestureDetector(
          child: SizedBox(
            width: 70.0,
            child: AnimatedSwitcher(
              duration: const Duration(milliseconds: 500),
              transitionBuilder: (Widget child, Animation<double> animation) {
                return ScaleTransition(
                  scale: animation,
                  alignment: Alignment.center,
                  filterQuality: FilterQuality.medium,
                  child: child,
                );
              },
              child: leadingWidget,
            ),
          ),
        ),
        title: Text(
          candidate.nom!,
          style: const TextStyle(
            fontWeight: FontWeight.w500,
          ),
        ),
        subtitle: Text("${candidate.region!} ${_buildStatutFinal(index)}"),
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => CandidatePage(candidate: candidate),
            ),
          );
        },
      ),
    );
  }
}

  